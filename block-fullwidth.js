/**
 * Author: JODE
 *
 * Copyright © 2024 by JODE. All rights reserved.
 *
 * This script is designed to handle blocks that need to be displayed across the full width of the browser window.
 * To use this script, add the 'block-fullwidth' class to the HTML elements you want to extend.
 *
 * How it works:
 * - The 'DOMContentLoaded' event is used to ensure that the script runs after the complete loading of the DOM.
 * - The function 'getViewportWidth' retrieves the current width of the viewport.
 * - 'applyWidthToElements' applies this width to all elements with the 'block-fullwidth' class.
 * - 'adjustLeftMargin' adjusts the left margin of these elements to align them properly with the parent container with class 'container' and fixed with '--width-container'.
 * - This script also handles window resizing to adjust the width and margin of the elements dynamically.
 *
 * Usage:
 * - Simply add the class 'block-fullwidth' to any block-level element in your HTML.
 * - No additional configuration is required.
 */

document.addEventListener("DOMContentLoaded", () => {
  const getViewportWidth = () =>
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;

  const applyWidthToElements = (width) => {
    const elements = document.querySelectorAll(".block-fullwidth");
    elements.forEach((el) => {
      el.style.width = `${width}px`;
    });
  };

  const adjustLeftMargin = () => {
    const currentWidth = getViewportWidth();
    const container = document.querySelector(".container");
    const containerWidth = parseInt(
      getComputedStyle(container).getPropertyValue("--width-container"),
      10
    );
    const marginLeft = (currentWidth - containerWidth) / 2;

    const elements = document.querySelectorAll(".block-fullwidth");
    elements.forEach((el) => {
      el.style.left = `-${marginLeft}px`;
    });
  };

  if (document.querySelector(".block-fullwidth")) {
    let currentWidth = getViewportWidth();
    applyWidthToElements(currentWidth);
    adjustLeftMargin();

    window.addEventListener("resize", () => {
      currentWidth = getViewportWidth();
      applyWidthToElements(currentWidth);
      adjustLeftMargin();
    });
  }
});
