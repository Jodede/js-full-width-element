# Full-Width Block Handler

## Overview

This JavaScript script, developed by JODE, is designed to extend specific HTML block elements across the full width of the browser window. It's an effective solution for developers looking to create responsive, full-width blocks in their web pages.

## Features

- **Dynamic Width Adjustment:** Automatically extends the width of elements to match the viewport.
- **Responsive Design:** Adjusts elements' width and margin upon window resizing.
- **Easy to Implement:** Simple integration with HTML through a single class addition.

## How It Works

The script operates in several key steps:

1. **Initialization:** Waits for the `DOMContentLoaded` event to ensure the DOM is fully loaded.
2. **Width Calculation:** Uses `getViewportWidth` to retrieve the current viewport width.
3. **Width Application:** Applies the calculated width to elements with the `block-fullwidth` class.
4. **Margin Adjustment:** Adjusts the left margin of these elements to align properly within a parent container.

## Usage

To apply this functionality to any block-level element:

1. Add the class `block-fullwidth` to the desired HTML elements.
2. Ensure the parent container has the class `container` with a fixed width set in the `--width-container` variable.
3. No additional JavaScript configuration is needed.

## License

Copyright © 2024 by JODE. All rights reserved.

## Author

JODE

Feel free to integrate this script into your web projects to effortlessly manage full-width blocks in a responsive manner.
